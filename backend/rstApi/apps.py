from django.apps import AppConfig


class RstapiConfig(AppConfig):
    name = 'rstApi'
