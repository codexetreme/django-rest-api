from django.shortcuts import render, HttpResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rstApi.models import Destination
from django.http import Http404
from rest_framework import status
from rstApi.serializer import DestinationSerializer

class DestinationDetail(APIView):
    """
    Retrieve, update or delete a destination instance.
    """
    def get_object(self, dest_id):
        try:
            return Destination.objects.get(pk=dest_id)
        except Destination.DoesNotExist:
            raise Http404

    def get(self, request, dest_id, format=None):
        destination = self.get_object(dest_id)
        serializer = DestinationSerializer(destination)
        return Response(serializer.data)

    def post(self, request, dest_id, format=None):
        destination = self.get_object(dest_id)
        serializer = DestinationSerializer(destination, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, dest_id, format=None):
        destination = self.get_object(dest_id)
        destination.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

# Create your views here.

def index(request):
    return HttpResponse("You're looking at question.")

