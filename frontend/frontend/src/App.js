import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';






function App() {

  const [data, setData] = useState({});

  useEffect(()=>{
    fetch('http://localhost:8000/rstApi/destination/1')
      .then((resp) => resp.json()) // Transform the data into json
      .then(function (data) {
        console.log(data);
        setData(data);
      });

  },[])

 

  return (
    <div className="App">
      <div>
        id: {data.id}
      </div>
      
      <div>
        name: {data.name}
      </div>

      <div>
        desc: {data.desc}
      </div>

    </div>
  );
}

export default App;
